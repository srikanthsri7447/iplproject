const fs = require('fs')
const csv = require('csvtojson')

filePath1 = '/home/srikanth/Documents/iplProject/src/data/deliveries.csv'
fileName1 = '/home/srikanth/Documents/iplProject/src/data/deliveries.json'

filePath2 = '/home/srikanth/Documents/iplProject/src/data/matches.csv'
fileName2 = '/home/srikanth/Documents/iplProject/src/data/matches.json'

function convertCsvToJson(filePath, fileName) {
    csv()
        .fromFile(filePath)
        .then((jsonObj) => {
            fs.writeFile(fileName, JSON.stringify(jsonObj), 'utf-8', (err) => {
                if (err) console.log(err)
            })
        })
}


convertCsvToJson(filePath1, fileName1)
convertCsvToJson(filePath2,fileName2)