let matches = require('../data/matches.json');
let deliveries = require('../data/deliveries.json');
const fs = require('fs');
const iplFunctions = require('./ipl');
const outputJson = require('./outputJson')

const iplYearBook = iplFunctions.matchesPerYear(matches);
const filePath1 = "src/public/output/matchesPerYear.json"
outputJson(filePath1, iplYearBook);

const iplResultBook = iplFunctions.matchesOwnPerYear(matches)
const filePath2 = 'src/public/output/noOfWinsPerYear.json'
outputJson(filePath2, iplResultBook)

const extraRuns = iplFunctions.extraRuns2016(deliveries, matches)
const filePath3 = 'src/public/output/runsPerTeamIn2016.json'
outputJson(filePath3, extraRuns)

const topEconomicBowlers = iplFunctions.ecoBowler2015(deliveries,matches)
const filePath4 = 'src/public/output/topEconomicalBowlersIn2015.json'
outputJson(filePath4,topEconomicBowlers)